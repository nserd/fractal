import javax.swing.*;
import java.awt.*;

class MainFrame extends JFrame {
    MainFrame() {
        int width  = 800;
        int height = 800;
        int scale  = 200;

        Container container = getContentPane();
        container.setLayout(new BorderLayout());

        PaintFrame grid = new PaintFrame();
        grid.setPreferredSize(new Dimension(width, height));

        container.add(grid);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocation(900, 200);
        pack();
        setVisible(true);

        PaintFrame.Figures figure = PaintFrame.Figures.MANDELBROT_SET;
        setTitle(figure.name());

        grid.draw(width, height, scale, figure);
    }

    public static void main(String[] args) {
        new MainFrame();
    }
}
