package grid;

import java.util.Objects;

public class ComplexNumber {
    private final double re;
    private final double im;

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public ComplexNumber(ComplexNumber complexNumber) {
        this.re = complexNumber.re;
        this.im = complexNumber.im;
    }

    public ComplexNumber add(ComplexNumber cNumber) {
        return new ComplexNumber(this.re + cNumber.getRe(), this.im + cNumber.getIm());
    }

    public double getMod() {
        return Math.sqrt(this.re * this.re + this.im * this.im);
    }

    private double getArg() {
        double arg = Math.atan(im / re);

        if (re < 0) {
            arg = im >= 0 ? arg + Math.PI : arg - Math.PI;
        }

        return arg;
    }

    public ComplexNumber pow(double power) {
        double factor = Math.pow(this.getMod(), power);
        double arg = this.getArg();

        double powRe = factor * Math.cos(power * arg);
        double powIm = factor * Math.sin(power * arg);

        return new ComplexNumber(powRe, powIm);
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ComplexNumber) {
            ComplexNumber cN = (ComplexNumber) o;
            return this.re == cN.re && this.im == cN.im;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(re, im);
    }

    @Override
    public String toString() {
        return "figures.ComplexNumber{" +
                "re=" + re +
                ", im=" + im +
                '}';
    }
}