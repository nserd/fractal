package grid;

import java.awt.*;
import java.util.List;
import java.util.Optional;

public class CoordinateGrid {
    private final int width;
    private final int height;
    private final int scale;

    private final double biasX;
    private final double biasY;

    private Color[][] grid;

    public CoordinateGrid(int width, int height, int scale, double biasX, double biasY) {
        // высота и ширина всегда должна быть нечетной для корректного отображения
        if (width % 2 == 0) width++;
        if (height % 2 == 0) height++;

        this.width = width;
        this.height = height;
        this.scale = scale;

        this.biasX = biasX;
        this.biasY = biasY;

        grid = new Color[height][width];
    }

    public CoordinateGrid(int width, int height, int scale) {
        this(width,height,scale, 0, 0);
    }

    /** Ставит точку на координатной плоскости
     *  @param x - координата по оси X
     *  @param y - координата по оси Y
     * */
    protected final void setPoint(double x, double y, Color color) {
        int indexI = coordinateY(scale * (y - biasY));
        int indexJ = coordinateX(scale * (x - biasX));

        if (indexI < height && indexI > -1 && indexJ < width && indexJ > -1) {
            grid[indexI][indexJ] = color;
        }
    }

    /** Рисует оси на координатной плоскости
     *  @param marks - разаделение на единичные отрезки
     *  @param axisColor - цвет линий
     * */
    public final void printAxis(boolean marks, Color axisColor) {
        int maxHeight = (height - 1) / 2;
        int maxWidth = (width - 1) / 2;

        for (double i = -maxHeight; i < maxHeight; i += (double)1 / scale) {
            setPoint(0, i, axisColor);
        }

        for (double i = -maxWidth; i < maxWidth; i += (double)1 / scale) {
            setPoint(i, 0, axisColor);
        }

        if (marks) {
            int yMarksLength = maxHeight / scale;
            int xMarksLength = maxWidth / scale;

            for (int i = 1; i <= yMarksLength; i++) {
                setPoint((double)1 / scale, i, axisColor);
                setPoint((double)-1 / scale, i, axisColor);
                setPoint((double)1 / scale, -i, axisColor);
                setPoint((double)-1 / scale, -i, axisColor);
            }

            for (int i = 1; i <= xMarksLength; i++) {
                setPoint(i, (double)1 / scale, axisColor);
                setPoint(i, (double)-1 / scale, axisColor);
                setPoint(-i, (double)1 / scale, axisColor);
                setPoint(-i, (double)-1 / scale, axisColor);
            }
        }
    }

    public void buildFigure(Figure figure) {
        int scale = this.getScale();

        double boundX = this.getWidth()  / (double)2 / scale;
        double boundY = this.getHeight() / (double)2 / scale;

        for (double i = -boundY + biasY; i < boundY + biasY; i = doubleIncrement(i)) {
            for (double j = -boundX + biasX; j < boundX + biasX; j = doubleIncrement(j)) {
                fillData(j, i, figure);
            }
        }
    }

    public void buildFigure(ComplexSetDensity density) {
        int scale = this.getScale();

        double boundX = this.getWidth()  / (double)2 / scale;
        double boundY = this.getHeight() / (double)2 / scale;

        for (double i = -boundY; i < boundY; i = doubleIncrement(i)) {
            for (double j = -boundX; j < boundX; j = doubleIncrement(j)) {
                fillData(j, i, density);
            }
        }
    }

    protected void fillData(double x, double y, Figure figure) {
        Optional.ofNullable(figure.getDot(x, y)).ifPresent(c -> {
            if (c instanceof Dot) {
                Dot dot = (Dot) c;
                setPoint(dot.getX(), dot.getY(), dot);
            } else {
                setPoint(x, y, c);
            }
        });
    }

    protected void fillData(double x, double y, ComplexSetDensity density) {
        List<Dot> dots = density.getDots(x, y);

        for (Dot dot : dots) {
            setPoint(dot.getX(), dot.getY(), dot);
        }
    }

    /** Возвращает инкрементированный счетчик. Нужно для обхода всей координатной плоскости с учетом маштаба.
     *  @param value - значение счетчика
     *  @param increment - значение инкремента
     * */
    protected final double doubleIncrement(double value, double increment) {
        double accuracy = this.getScale() * 10;
        double sum = value + increment;

        return Math.floor(sum * accuracy) / accuracy;
    }

    protected final double doubleIncrement(double value) {
        return doubleIncrement(value, (double)1 / this.getScale());
    }

    private int coordinateX(double x) {
        return (int) (x + (width - 1) / 2);
    }

    private int coordinateY(double y) {
        return (int) (-y + (height - 1) / 2);
    }

    protected final int getScale() {
        return scale;
    }

    protected final int getWidth() {
        return width;
    }

    protected final int getHeight() {
        return height;
    }

    protected void clearGrid() {
        grid = new Color[width][height];
    }

    public Color[][] getFigure() {
        return grid;
    }
}
