package grid;

import java.awt.*;

@FunctionalInterface
public interface Figure {
    /**
     * Метод, возвращающий цвет точки,
     * которая будет располагаться на координатной плоскости.
     *
     * Если фигура лежит вне координат, указанных в параметрах, то метод возращает null
     *
     * @param x - координата по оси X
     * @param y - координата по оси Y
     */
    Color getDot(double x, double y);
}
