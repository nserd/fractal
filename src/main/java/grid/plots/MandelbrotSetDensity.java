package grid.plots;

import grid.ComplexNumber;
import grid.ComplexSetDensity;
import grid.Dot;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class MandelbrotSetDensity implements ComplexSetDensity {
    private static final int iterationNumb = 256;
    private static final int radius = 2;

    @Override
    public List<Dot> getDots(double x, double y) {
        List<Dot> dotList = new LinkedList<>();

        ComplexNumber cn = new ComplexNumber(x, y);
        ComplexNumber z  = new ComplexNumber(cn);

        for (int k = 0; k < iterationNumb; k++) {
            z = z.pow(2).add(cn);

            Color color = new Color(k, unsignedByte(x * k), unsignedByte(y * k));
            dotList.add(new Dot(z.getRe(), z.getIm(), color));

            if (z.getMod() > radius) {
                break;
            }
        }

        return dotList;
    }

    private int unsignedByte(double d) {
        return (byte) d + 128;
    }
}
