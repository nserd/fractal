package grid.plots;

import grid.Dot;
import grid.Figure;

import java.awt.*;
import java.util.stream.DoubleStream;

/**
 * Каскад бифуркаций (Последовательность Фейгенбаума)
 */
public class Bifurcation implements Figure {

    @Override
    public Color getDot(double x, double y) {
        return y > 0 ? new Dot(y, equilibriumValue(x, y), Color.BLACK) : null;
    }

    private double equilibriumValue(double x0, double r) {
        return DoubleStream.iterate(x0, x -> r * x * (1 - x))
                .limit(1000)
                .reduce((x1, x2) -> x2).orElse(0);
    }
}
