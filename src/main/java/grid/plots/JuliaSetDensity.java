package grid.plots;

import grid.ComplexNumber;
import grid.ComplexSetDensity;
import grid.Dot;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class JuliaSetDensity implements ComplexSetDensity {
    private static final int iterationNumb = 256;
    private static final int radius = 2;

    @Override
    public List<Dot> getDots(double x, double y) {
        List<Dot> dotList = new LinkedList<>();
        ComplexNumber cN = new ComplexNumber(x, y);

        for (int k = 0; k < iterationNumb; k++) {
            cN = cN.pow(2);
            cN = new ComplexNumber(cN.getRe() - 0.8, cN.getIm());

            Color color = new Color(k, k, k);
            dotList.add(new Dot(cN.getRe(), cN.getIm(), color));

            if (cN.getMod() > radius) {
                break;
            }
        }

        return dotList;
    }
}
