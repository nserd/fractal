package grid.plots;

import grid.Dot;
import grid.Figure;

import java.awt.*;

public class Sinus implements Figure {
    @Override
    public Color getDot(double x, double y) {
        return new Dot(x, Math.sin(x), Color.BLACK);
    }

}
