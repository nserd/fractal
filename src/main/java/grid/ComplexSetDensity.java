package grid;

import java.util.List;


public interface ComplexSetDensity {
    /**
     * Метод, возвращающий список из точек,
     * которые будут располагаться на координатной плоскости.
     *
     * Новые точки формируются на основе координат, которые передаются в параметрах метода.
     *
     * @param x - координата по оси X
     * @param y - координата по оси Y
     */
    List<Dot> getDots(double x, double y);
}
