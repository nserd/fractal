package grid;

import java.awt.*;

/**
 * Класс Dot, расширяет класс Color, добавляя к нему координаты. Используется в тех случаях,
 * когда нужно передать информацию не только о цвете точки, но и о её новом расположении.
 * */
public class Dot extends Color {
    private final double coordinateX;
    private final double coordinateY;

    public Dot(double coordinateX, double coordinateY, Color color) {
        super(color.getRGB());

        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public double getX() {
        return coordinateX;
    }

    public double getY() {
        return coordinateY;
    }
}
