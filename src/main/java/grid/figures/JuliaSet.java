package grid.figures;

import grid.ComplexNumber;
import grid.Figure;

import java.awt.*;

public class JuliaSet implements Figure {
    private static final int colorNumb = 100;
    private static final int radius = 2; // множество Жюлиа полностью лежит в радиусе 2.

    @Override
    public Color getDot(double x, double y) {
        ComplexNumber cN = new ComplexNumber(x, y);

        int value = belongsToSet(cN);

        if (value == -1) {
            return Color.BLACK;
        } else {
            int red = 255 / colorNumb * value;
            int green = 255 / colorNumb * value;
            int blue = 92;

            return new Color(red, green, blue);
        }
    }

    private int belongsToSet(ComplexNumber cN) {
        for (int k = 0; k < colorNumb; k++) {
            cN = cN.pow(2);
            cN = new ComplexNumber(cN.getRe() - 0.8, cN.getIm() );

            if (cN.getMod() > radius) {
                return k;
            }
        }

        return -1;
    }
}