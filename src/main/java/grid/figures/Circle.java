package grid.figures;

import grid.Figure;

import java.awt.*;

public class Circle implements Figure {

    @Override
    public Color getDot(double x, double y) {
        return x * x + y * y <= 1 ? Color.BLACK : null;
    }
}
