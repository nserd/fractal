package grid.figures;

import grid.ComplexNumber;
import grid.Figure;

import java.awt.*;

public class MandelbrotSet implements Figure {
    private static final int colorNumb = 100;
    private static final int radius = 2;

    @Override
    public Color getDot(double x, double y) {
        ComplexNumber cNumber = new ComplexNumber(x, y);

        int value = belongsToSet(cNumber);

        if (value == -1) {
            return Color.BLACK;
        } else {
            int red = 255 / colorNumb * value;
            int green = 255 / colorNumb * value;
            int blue = 92;

            return new Color(red, green, blue);
        }
    }

    private int belongsToSet(ComplexNumber c) {
        ComplexNumber z = new ComplexNumber(c);

        for (int i = 1; i < colorNumb; i++) {
            z = z.pow(2).add(c);

            if (z.getMod() > radius) {
                return i;
            }
        }

        return -1;
    }
}

// scale = 1000000000, biasX = -1.48360074, biasY = 0.0001001