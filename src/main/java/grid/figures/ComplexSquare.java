package grid.figures;

import grid.ComplexNumber;
import grid.Dot;
import grid.Figure;

import java.awt.*;

public class ComplexSquare implements Figure {
    @Override
    public Dot getDot(double x, double y) {
        ComplexNumber cN = new ComplexNumber(x, y).pow(0.9);
        return new Dot(cN.getRe(), cN.getIm(), Color.RED);
    }
}
