import grid.*;
import grid.figures.*;
import grid.plots.Bifurcation;
import grid.plots.JuliaSetDensity;
import grid.plots.MandelbrotSetDensity;
import grid.plots.Sinus;

import javax.swing.*;
import java.awt.*;

public class PaintFrame extends JPanel {
    private final int frames;

    private CoordinateGrid grid;

    private Color[][][] data;

    private int counter = 0;

    private boolean toggle = true;

    PaintFrame(int frames) {
        this.frames = frames;
    }

    PaintFrame() {
        this(0);
    }

    public void draw(int width, int height, int scale, Figures figures) {
        grid = new CoordinateGrid(width, height, scale);
        
        switch (figures) {
            case CIRCLE         -> grid.buildFigure(new Circle());
            case COMPLEX_SQUARE -> grid.buildFigure(new ComplexSquare());
            case JULIA_SET      -> grid.buildFigure(new JuliaSet());
            case MANDELBROT_SET -> grid.buildFigure(new MandelbrotSet());

            case JULIA_SET_DENSITY      -> grid.buildFigure(new JuliaSetDensity());
            case MANDELBROT_SET_DENSITY -> grid.buildFigure(new MandelbrotSetDensity());
            case BIFURCATION -> grid.buildFigure(new Bifurcation());
            case SINUS -> grid.buildFigure(new Sinus());
        }


        grid.printAxis(true, Color.BLUE);

        if (frames > 0) {
            data = new Color[frames][][];

            System.out.print("Load data ");
            for (int i = 0; i < frames; i++) {
                data[i] = grid.getFigure();
                System.out.print(".");
            }
            System.out.println(" Done.");
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (frames <= 0 && grid != null ) {
            Color[][] points = grid.getFigure();
            g.clearRect(0, 0, points[0].length, points.length);
            paintFigure(points, g);
        }

        if (frames > 0 && data != null) {
            if (data[data.length - 1] != null) {
                Color[][] points = data[counter];
                g.clearRect(0, 0, points[0].length, points.length);
                paintFigure(points, g);

                if (counter - 1 < 0) { toggle = true; }
                if (counter + 1 > data.length - 1) { toggle = false; }

                counter = toggle ? counter + 1 : counter - 1;
            }
        }

        repaint();
    }

    private void paintFigure(Color[][] points, Graphics g) {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points[i].length; j++) {
                if (points[i][j] != null) {
                    g.setColor(points[i][j]);
                    g.fillRect(j, i, 1, 1);
                }
            }
        }
    }

    enum Figures {
        COMPLEX_SQUARE,
        JULIA_SET,
        JULIA_SET_DENSITY,
        MANDELBROT_SET,
        MANDELBROT_SET_DENSITY,
        CIRCLE,
        BIFURCATION,
        SINUS
    }
}
